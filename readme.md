
## Marvelous App  
  
To run the app, do the following steps:   

 *considering that you have the React Native environment set*
 
- get your Marvel API key (developer.marvel.com) and then replace the values in .env file
- run npm install in project root directory (all native libs should be auto linked)
- run npm run android

### Considerations
- the icons copyrights are totally of the they respective owners
- the image assets should better optimized and maybe remote to reduce the bundle size
- add some import alias to reduce the resource path size
