import AsyncStorage from '@react-native-community/async-storage';
import {MARVEL_API_KEY, MARVEL_HASH_KEY} from 'react-native-dotenv';
import axios from './api';

const COMICS_ASYNCSTORAGE_KEYS = {
  FAVORITE_HERO: 'FAVORITE_HERO',
};

export async function getHeroByName(name) {
  try {
    const {data} = await axios.get(
      `/characters?ts=1&nameStartsWith=${name}&apikey=${MARVEL_API_KEY}&hash=${MARVEL_HASH_KEY}`,
    );
    return data.data.results;
  } catch (e) {
    throw new Error('Fail to get characters');
  }
}

export async function getHeroComics(hero) {
  try {
    const {data} = await axios.get(
      `characters/${hero}/comics?ts=1&apikey=${MARVEL_API_KEY}&hash=${MARVEL_HASH_KEY}`,
    );
    return data.data.results;
  } catch (e) {
    throw new Error('Fail to get character comics');
  }
}

export async function saveFavoriteHero(hero) {
  try {
    await AsyncStorage.setItem(
      COMICS_ASYNCSTORAGE_KEYS.FAVORITE_HERO,
      JSON.stringify(hero),
    );
  } catch (e) {
    throw new Error('Fail to set favorite character');
  }
}

export async function getFavoriteHero() {
  try {
    const data = await AsyncStorage.getItem(
      COMICS_ASYNCSTORAGE_KEYS.FAVORITE_HERO
    );

    return JSON.parse(data);
  } catch (e) {
    throw new Error('Fail to get favorite character');
  }
}
