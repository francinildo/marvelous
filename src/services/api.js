import * as axios from 'axios';
import {MARVEL_API_BASE_URL} from 'react-native-dotenv';


const instance = axios.create({
  baseURL: MARVEL_API_BASE_URL,
  timeout: 1000,
});

export default instance;
