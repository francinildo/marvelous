import React, {useState, useEffect} from 'react';
import {StatusBar, FlatList, ActivityIndicator} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Box, {
  BoxDimensions,
  BoxMargins,
  BoxPadding,
} from '../../components/core/Box';
import {withTheme} from 'styled-components';
import SearchInput from './components/SearchInput';
import {getHeroByName, saveFavoriteHero} from '../../services/HeroService';
import SearchResultItem from './components/SearchResultItem';
import {ResultsLabel, SearchLabel, NoResultsLabel} from './styles';
import {getFavoriteHeroComics} from '../../store/ducks/comics';

function SelectHero(props) {
  const {theme, navigation} = props;
  const {colors} = theme;
  const characterInitialState = [];

  const dispatch = useDispatch();

  const [characterList, setCharacters] = useState(characterInitialState);
  const [loading, setLoading] = useState(false);

  const hasCharacters = characterList.length > 0;

  async function getCharacters(query) {
    try {
      if (query.length >= 3) {
        setLoading(true);
        const characters = await getHeroByName(query);
        setLoading(false);
        setCharacters(characters);
      } else {
        if (hasCharacters) {
          setCharacters(characterInitialState);
        }
      }
    } catch (e) {
      setLoading(false);
    }
  }

  async function setFavoriteCharacter(character) {
    await saveFavoriteHero(character);
    dispatch(getFavoriteHeroComics(character));
    navigation.goBack();
  }

  function getContent() {
    if (loading) {
      return (
        <Box flex={1} absoluteCenterChildren>
          <ActivityIndicator color={colors.black} />
        </Box>
      );
    }
    return (
      <Box
        flex={1}
        margin={BoxMargins.only({top: 32})}
        padding={BoxPadding.symmetricX(12)}>
        {hasCharacters ? (
          <>
            <ResultsLabel>Results:</ResultsLabel>
            <FlatList
              data={characterList}
              keyExtractor={item => item.id}
              renderItem={({item}) => (
                <SearchResultItem
                  onItemSelected={setFavoriteCharacter}
                  character={item}
                />
              )}
            />
          </>
        ) : (
          <Box flex={1} absoluteCenterChildren>
            <NoResultsLabel>No results</NoResultsLabel>
          </Box>
        )}
      </Box>
    );
  }

  return (
    <Box
      width={BoxDimensions.matchParent}
      height={BoxDimensions.matchParent}
      bgColor={colors.white}>
      <StatusBar backgroundColor={colors.white} animated />
      <Box
        width={BoxDimensions.matchParent}
        height={86}
        margin={BoxMargins.only({top: 12})}
        padding={BoxPadding.only({top: 12, bottom: 12, right: 12, left: 12})}>
        <Box margin={BoxMargins.only({bottom: 12})}>
          <SearchLabel>Search Heroes</SearchLabel>
        </Box>
        <SearchInput debounceTime={200} onText={text => getCharacters(text)} />
      </Box>
      {getContent()}
    </Box>
  );
}

export default withTheme(SelectHero);
