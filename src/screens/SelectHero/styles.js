import styled from 'styled-components';

export const SearchLabel = styled.Text`
  font-size: 28px;
  font-family: 'Marvel-Bold';
`;

export const ResultsLabel = styled.Text`
  font-size: 22px;
  font-family: 'Marvel-Regular';
`;

export const NoResultsLabel = styled.Text`
  font-size: 22px;
  font-family: 'Marvel-Regular';
  color: ${({theme}) => theme.colors.grey};
`;
