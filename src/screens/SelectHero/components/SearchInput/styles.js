import styled from 'styled-components/native';
import Box, {BoxDimensions, BoxPadding} from '../../../../components/core/Box';

export const Container = styled(Box).attrs(({theme}) => ({
  width: BoxDimensions.matchParent,
  height: 52,
  bgColor: theme.colors.white,
  padding: BoxPadding.symmetricX(12),
}))`
  border-width: 0.5px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-top: 2px;
  border-radius: 2px;
`;
export const Input = styled.TextInput`
  font-size: 24px;
  font-family: 'Marvel-Bold';
`;

