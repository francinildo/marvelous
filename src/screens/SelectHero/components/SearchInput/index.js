import React from 'react';
import {withTheme} from 'styled-components';
import debounce from 'debounce';
import {Container, Input} from './styles';

function SearchInput(props) {
  const {theme, onText, debounceTime = 0} = props;
  const {colors} = theme;

  const onChangeText = debounce(onText, debounceTime);

  return (
    <Container>
      <Input
        placeholder={'Search your favorite hero'}
        onChangeText={text => onChangeText(text)}
      />
    </Container>
  );
}
export default withTheme(SearchInput);
