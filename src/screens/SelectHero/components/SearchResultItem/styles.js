import styled from 'styled-components/native';
import Box, {BoxDimensions, BoxPadding} from '../../../../components/core/Box';

export const Container = styled(Box).attrs(({theme}) => ({
  width: BoxDimensions.matchParent,
  height: 72,
  bgColor: theme.colors.white,
  padding: BoxPadding.symmetricX(0),
}))`
  border-bottom-width: 0.5px;
  flex-direction: row;
  align-items: center;
  padding-top: 12px;
  padding-bottom: 12px;
`;
export const CharacterName = styled.Text`
  font-size: 24px;
  font-family: 'Marvel-Bold';
`;

export const CharacterAvailableComics = styled.Text`
  font-size: 18px;
  font-family: 'Marvel-Regular';
`;

export const CharacterThumb = styled.Image`
  width: 52px;
  height: 52px;
  border-radius: 2px;
`;
