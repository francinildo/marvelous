import React from 'react';
import {withTheme} from 'styled-components';
import {
  Container,
  CharacterName,
  CharacterThumb,
  CharacterAvailableComics,
} from './styles';
import Box, {BoxMargins} from '../../../../components/core/Box';

function SearchResultItem(props) {
  const {character, onItemSelected} = props;
  const {name, thumbnail, comics} = character;
  const {available} = comics;
  const {path, extension} = thumbnail;
  const imagePath = `${path}.${extension}`;

  const comicsAvailable = `${available} comics available`;

  return (
    <Container onPress={() => onItemSelected(character)}>
      <CharacterThumb source={{uri: imagePath}} />
      <Box margin={BoxMargins.only({left: 22})}>
        <CharacterName>{name}</CharacterName>
        <CharacterAvailableComics>{comicsAvailable}</CharacterAvailableComics>
      </Box>
    </Container>
  );
}

export default withTheme(SearchResultItem);
