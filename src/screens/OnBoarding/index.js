/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {StatusBar, ActivityIndicator} from 'react-native';
import {useDispatch} from 'react-redux';
import {withTheme} from 'styled-components';
import {useIsFocused, StackActions} from '@react-navigation/native';
import {
  AppLogo,
  AppLogoLabel,
  Container,
  Footer,
  Illustration,
  IllustrationContainer,
  MessageContainer,
  WelcomeMessage,
  WelcomeMessageContainer,
  WelcomeTitle,
} from './styles';
import Button, {ButtonType} from '../../components/Button';
import {getFavoriteHero} from '../../services/HeroService';
import {setFavoriteHero} from '../../store/ducks/comics';

function OnBoarding(props) {
  const {theme} = props;
  const {colors} = theme;

  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();
  const focused = useIsFocused();
  const {navigation} = props;

  function gotToSelectHeroScreen() {
    navigation.push('SelectHero');
  }

  async function loadHero() {
    setLoading(true);
    const hero = await getFavoriteHero();
    if (hero && focused) {
      dispatch(setFavoriteHero(hero));
      setTimeout(() => {
        setLoading(false);
        navigation.dispatch(StackActions.replace('ComicsHome'));
      }, 2000);
    } else {
      setLoading(false);
    }
  }

  // todo: just for tests, before that, remove this logic and define it in another place
  useEffect(() => {
    loadHero();
  }, [focused]);

  return (
    <Container>
      <StatusBar
        backgroundColor={colors.homeIllustration}
        barStyle={'dark-content'}
        animated
      />
      <IllustrationContainer>
        <Illustration />
      </IllustrationContainer>
      <MessageContainer>
        <AppLogo>
          <AppLogoLabel>MARVELOUS</AppLogoLabel>
        </AppLogo>
        <WelcomeMessageContainer>
          <WelcomeTitle>Welcome to our comics app</WelcomeTitle>
          <WelcomeMessage>We hope you enjoy</WelcomeMessage>
        </WelcomeMessageContainer>
        <Footer>
          {loading ? (
            <ActivityIndicator color={colors.white} size={'large'} />
          ) : (
            <Button
              label={'Get started'}
              type={ButtonType.NORMAL}
              onPress={() => gotToSelectHeroScreen()}
            />
          )}
        </Footer>
      </MessageContainer>
    </Container>
  );
}

export default withTheme(OnBoarding);
