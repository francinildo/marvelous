import styled from 'styled-components/native';
import {ImageResources} from '../../theme';

export const Container = styled.View`
  height: 100%;
  width: 100%;
  background-color: ${({theme}) => theme.colors.homeIllustration};
`;

export const IllustrationContainer = styled.View`
  flex: 1.4;
  align-items: center;
  justify-content: center;
`;

export const Illustration = styled.Image.attrs({
  source: ImageResources.ironMan,
  resizeMode: 'contain',
})`
  width: 100%;
  margin-top: 180px;
  transform: scale(2);
`;

export const MessageContainer = styled.View`
  background-color: ${({theme}) => theme.colors.homeIllustrationDark};
  flex: 1;
  justify-content: space-between;
`;

export const WelcomeMessageContainer = styled.View``;

export const AppLogo = styled.View`
  width: 200px;
  height: 48px;
  align-self: center;
  align-items: center;
  justify-content: center;
  margin-top: 28px;
`;

export const WelcomeTitle = styled.Text`
  font-size: 26px;
  font-family: 'Marvel-Regular';
  text-align: center;
  color: ${({theme}) => theme.colors.white};
`;

export const WelcomeMessage = styled.Text`
  font-size: 22px;
  font-family: 'Marvel-Regular';
  text-align: center;
  color: ${({theme}) => theme.colors.white};
`;

export const AppLogoLabel = styled.Text`
  font-size: 46px;
  font-family: 'Marvel-Bold';
  color: ${({theme}) => theme.colors.white};
`;

export const Footer = styled.View`
  align-items: center;
  justify-content: space-between;
  padding: 18px 0;
`;
