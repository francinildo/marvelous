import React from 'react';
import {BoxDimensions, BoxMargins} from '../../../../components/core/Box';
import {
  Container,
  CharacterThumb,
  ComicName,
  IssueNumber,
  ComicsPrice,
  LabelsContainer,
} from './styles';

function ComicListItem(props) {
  const {comic} = props;
  const {thumbnail, title, issueNumber, prices} = comic;
  const [price] = prices;
  const {path, extension} = thumbnail;
  const thumbPath = `${path}.${extension}`;

  return (
    <Container
      width={BoxDimensions.matchParent}
      height={160}
      margin={BoxMargins.only({bottom: 22})}>
      <CharacterThumb source={{uri: thumbPath}} />
      <LabelsContainer
        margin={BoxMargins.only({left: 12})}
        width={BoxDimensions.matchParent}>
        <ComicName>{title}</ComicName>
        <IssueNumber>#{issueNumber}</IssueNumber>
        <ComicsPrice>Price: ${price.price}</ComicsPrice>
      </LabelsContainer>
    </Container>
  );
}

export default ComicListItem;
