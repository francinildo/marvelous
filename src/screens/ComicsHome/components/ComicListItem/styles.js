import styled from 'styled-components/native/dist/styled-components.native.esm';
import Box from '../../../../components/core/Box';

export const Container = styled(Box)`
  flex-direction: row;
`;

export const LabelsContainer = styled(Box)`
  flex-shrink: 1;
`;

export const ComicName = styled.Text`
  font-size: 24px;
  font-family: 'Marvel-Bold';
  max-width: 100%;
`;

export const ComicsPrice = styled.Text`
  font-size: 18px;
  font-family: 'Marvel-Regular';
  position: absolute;
  bottom: 0;
`;

export const IssueNumber = styled.Text`
  font-size: 18px;
  font-family: 'Marvel-Regular';
`;

export const CharacterThumb = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 102px;
  border-radius: 2px;
`;
