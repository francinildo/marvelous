import styled from 'styled-components/native/dist/styled-components.native.esm';

export const HeaderLabel = styled.Text`
  font-size: 32px;
  font-family: 'Marvel-Bold';
  color: black;
`;
export const ResultsLabel = styled.Text`
  margin-top: 10px;
  font-size: 22px;
  font-family: 'Marvel-Regular';
  color: black;
`;

export const ChangeHeroLabel = styled.Text`
  margin-top: 10px;
  font-size: 16px;
  font-family: 'Marvel-Bold';
  color: black;
`;
