/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {StatusBar, FlatList, ActivityIndicator} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import ComicListItem from './components/ComicListItem';
import {HeaderLabel, ResultsLabel, ChangeHeroLabel} from './styles';
import {
  BoxAlignments,
  BoxDimensions,
  BoxDirections,
  BoxMargins,
  BoxPadding,
} from '../../components/core/Box';
import Box from '../../components/core/Box';
import {getFavoriteHeroComics, setFavoriteHero} from '../../store/ducks/comics';
import {withTheme} from 'styled-components';
import {getFavoriteHero, saveFavoriteHero} from '../../services/HeroService';
import {useIsFocused} from '@react-navigation/native';

function ComicsHome({theme, navigation}) {
  const {colors} = theme;

  const dispatch = useDispatch();
  const favoriteHero = useSelector(state => state.comics.favoriteHero);
  const loading = useSelector(state => state.comics.loading);
  const focused = useIsFocused();
  const favoriteHeroComics = useSelector(
    state => state.comics.favoriteHeroComics,
  );

  async function getHeroComics() {
    const hero = await getFavoriteHero();
    dispatch(setFavoriteHero(hero));
    dispatch(getFavoriteHeroComics(hero));
  }

  function changeHero() {
    navigation.push('SelectHero');
  }

  useEffect(() => {
    if (favoriteHero) {
      getHeroComics();
    }
  }, [focused]);

  return (
    <Box
      width={BoxDimensions.matchParent}
      height={BoxDimensions.matchParent}
      bgColor={colors.white}>
      <StatusBar backgroundColor={colors.white} barStyle={'dark-content'} />
      <Box
        margin={BoxMargins.symmetricY(18)}
        padding={BoxPadding.symmetricX(18)}>
        <Box
          flexDirection={BoxDirections.row}
          alignItems={BoxAlignments.center}
          justifyContent={BoxAlignments.spaceBetween}>
          <HeaderLabel>Marvelous</HeaderLabel>
          <Box
            onPress={() => {
              changeHero();
            }}
            padding={BoxPadding.only({top: 2, bottom: 2, left: 6, right: 6})}>
            <ChangeHeroLabel>Change hero</ChangeHeroLabel>
          </Box>
        </Box>
        <ResultsLabel>{favoriteHero.name}'s comics:</ResultsLabel>
        {favoriteHeroComics && (
          <Box
            margin={BoxMargins.only({top: 28, bottom: 102})}
            absoluteCenterChildren>
            {loading ? (
              <ActivityIndicator color={colors.black} size={'large'} />
            ) : (
              <FlatList
                data={favoriteHeroComics}
                renderItem={({item}) => <ComicListItem comic={item} />}
              />
            )}
          </Box>
        )}
      </Box>
    </Box>
  );
}

export default withTheme(ComicsHome);
