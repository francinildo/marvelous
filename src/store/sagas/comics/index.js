import {call, put, takeLatest} from 'redux-saga/effects';
import {
  setHeroComics,
  setHeroComicsLoading,
  Types,
} from '../../../store/ducks/comics/index';
import {getHeroComics} from '../../../services/HeroService';

function* fetchComics(action) {
  const {payload} = action;
  const {id} = payload;
  try {
    yield put(setHeroComicsLoading(true));
    const heroComics = yield call(getHeroComics, id);
    yield put(setHeroComics(heroComics));
    yield put(setHeroComicsLoading(false));
  } catch (e) {
    yield put(setHeroComicsLoading(false));
  }
}

function* comicsFlow() {
  yield takeLatest(Types.GET_HERO_COMICS, fetchComics);
}

export default comicsFlow;
