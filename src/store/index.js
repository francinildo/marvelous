import {combineReducers, createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import comics from './ducks/comics';
import comicsSaga from './sagas/comics';

const reducers = combineReducers({
  comics,
});

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(comicsSaga);

export default store;
