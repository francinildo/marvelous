// Action Types
export const Types = {
  SET_FAVORITE_HERO: 'comics/SET_FAVORITE_HERO',
  SET_HERO_COMICS: 'comics/SET_HERO_COMICS',
  GET_HERO_COMICS: 'comics/GET_HERO_COMICS',
  HERO_COMICS_LOADING: 'comics/HERO_COMICS_LOADING',
};

// Reducer
const initialState = {
  favoriteHero: null,
  favoriteHeroComics: null,
  loading: false,
};

export default function reducer(state = initialState, action) {
  const {payload} = action;
  switch (action.type) {
    case Types.SET_FAVORITE_HERO:
      return {...state, favoriteHero: payload};
    case Types.HERO_COMICS_LOADING:
      return {...state, loading: payload};
    case Types.SET_HERO_COMICS:
      return {...state, favoriteHeroComics: payload};
    default:
      return state;
  }
}

// Action Creators

export function setFavoriteHero(hero) {
  return {
    type: Types.SET_FAVORITE_HERO,
    payload: hero,
  };
}

export function getFavoriteHeroComics(hero) {
  return {
    type: Types.GET_HERO_COMICS,
    payload: hero,
  };
}

export function setHeroComicsLoading(loading) {
  return {
    type: Types.HERO_COMICS_LOADING,
    payload: loading,
  };
}

export function setHeroComics(comics) {
  return {
    type: Types.SET_HERO_COMICS,
    payload: comics,
  };
}
