import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ComicsHome from '../screens/ComicsHome';
import OnBoarding from '../screens/OnBoarding';
import SelectHero from '../screens/SelectHero';

const Stack = createStackNavigator();

function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen name={'OnBoarding'} component={OnBoarding} />
        <Stack.Screen name={'SelectHero'} component={SelectHero} />
        <Stack.Screen name={'ComicsHome'} component={ComicsHome} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Router;
