const colors = {
  white: '#ffffff',
  black: '#000000',
  primary: '#e23636',
  homeIllustration: '#a41117',
  homeIllustrationDark: '#160607',
  transparent: 'transparent',
  muted: 'rgba(0,0,0,0.1)',
  grey: 'grey',
};

export const ImageResources = {
  ironMan: require('../../assets/images/iron-man-avengers.jpg'),
};

export default {
  colors,
};
