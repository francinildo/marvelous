import styled from 'styled-components/native';
import {ButtonType} from './';

function pickContainerColor(disabled, theme, type) {
  if (disabled) {
    return theme.colors.disabled;
  }
  switch (type) {
    case ButtonType.NORMAL:
      return theme.colors.white;
    case ButtonType.COLORED:
      return theme.colors.primary;
    default:
      return theme.colors.transparent;
  }
}

function pickLabelColor(theme, type) {
  switch (type) {
    case ButtonType.NORMAL:
      return theme.colors.black;
    case ButtonType.COLORED:
      return theme.colors.white;
    default:
      return theme.colors.black;
  }
}

export const ButtonContainer = styled.TouchableOpacity.attrs(props => ({
  pointerEvents: props.disabled ? 'none' : 'auto',
}))`
  background-color: ${({disabled = false, theme, type}) =>
    pickContainerColor(disabled, theme, type)};
  min-width: 120px;
  height: 42px;
  justify-content: center;
  align-items: center;
  border-radius: 2px;
  elevation: 2;
  padding: 0 18px;
`;

export const ButtonLabel = styled.Text`
  font-family: 'Marvel-Bold';
  font-size: 18px;
  color: ${({theme, type}) => pickLabelColor(theme, type)};
`;
