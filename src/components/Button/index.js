import React from 'react';
import { ButtonContainer, ButtonLabel } from './styles';

export const ButtonType = {
  NORMAL : 0,
  COLORED: 1,
}
const Button = (props) => {
  const { label } = props;
  return (
    <ButtonContainer {...props}>
      <ButtonLabel {...props}>{label}</ButtonLabel>
    </ButtonContainer>
  );
};

Button.defaultProps = {
  type: ButtonType.NORMAL,
};

export default Button;
