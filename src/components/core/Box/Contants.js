export const BoxDimensions = Object.freeze({
  matchParent: '100%',
});

export const BoxAlignments = Object.freeze({
  center: 'center',
  flexStart: 'flex-start',
  spaceEvenly: 'space-evenly',
  spaceBetween: 'space-between',
});

export const BoxDirections = Object.freeze({
  row: 'row',
  column: 'column',
});

export const BoxMargins = Object.freeze({
  only: ({top = 0, right = 0, bottom = 0, left = 0}) => {
    return {
      marginTop: top,
      marginRight: right,
      marginBottom: bottom,
      marginLeft: left,
    };
  },
  symmetricX: size => {
    return BoxMargins.only({left: size, right: size});
  },
  symmetricY: size => {
    return BoxMargins.only({top: size, bottom: size});
  },
});

export const BoxPadding = Object.freeze({
  only: ({top = 0, right = 0, bottom = 0, left = 0}) => {
    return {
      paddingTop: top,
      paddingRight: right,
      paddingBottom: bottom,
      paddingLeft: left,
    };
  },
  symmetricX: size => {
    return BoxPadding.only({left: size, right: size});
  },
  symmetricY: size => {
    return BoxPadding.only({top: size, bottom: size});
  },
});

export const BoxInteractions = Object.freeze({
  noneIf: (condition = false) => (condition ? 'none' : 'auto'),
});
