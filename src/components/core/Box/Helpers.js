import { BoxAlignments } from "./Contants";

export function resolveAlignments(props) {
  const { absoluteCenterChildren, justifyContent, alignItems } = props;

  if (!absoluteCenterChildren)
    return {
      justifyContent,
      alignItems,
    };

  return {
    justifyContent: BoxAlignments.center,
    alignItems: BoxAlignments.center,
  };
}
