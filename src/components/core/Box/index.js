import React from 'react';
import PropTypes from 'prop-types';
import {
  BoxMargins,
  BoxPadding,
  BoxAlignments,
  BoxDirections,
  BoxInteractions,
  BoxDimensions,
} from './Contants';
import {resolveAlignments} from './Helpers';
import {Container, TouchableContainer} from './styles';

function Box(props) {
  const {
    width,
    height,
    padding,
    margin,
    onPress,
    flex,
    flexDirection,
    bgColor,
    ...rest
  } = props;

  const SelectedContainer = onPress ? TouchableContainer : Container;

  const {alignItems, justifyContent} = resolveAlignments(props);

  return (
    <SelectedContainer
      width={width}
      height={height}
      flex={flex}
      onPress={onPress}
      flexDirection={flexDirection}
      bgColor={bgColor}
      {...padding}
      {...margin}
      {...rest}
      alignItems={alignItems}
      justifyContent={justifyContent}
    />
  );
}

Box.propTypes = {
  absoluteCenterChildren: PropTypes.oneOf([PropTypes.boolean]),
  width: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
  flex: PropTypes.oneOf([PropTypes.number]),
  onClick: PropTypes.oneOf([PropTypes.func]),
  padding: PropTypes.oneOf([PropTypes.object]),
  margin: PropTypes.oneOf([PropTypes.object]),
  flexDirection: PropTypes.oneOf([PropTypes.string]),
  alignItems: PropTypes.oneOf([PropTypes.string]),
  justifyContent: PropTypes.oneOf([PropTypes.string]),
  bgColor: PropTypes.oneOf([PropTypes.string]),
  hidden: PropTypes.oneOf([PropTypes.boolean]),
};

Box.defaultProps = {
  width: 'auto',
  height: 'auto',
  padding: BoxMargins.only({top: 0, bottom: 0, left: 0, right: 0}),
  margin: BoxMargins.only({top: 0, bottom: 0, left: 0, right: 0}),
  hidden: false,
  flexDirection: 'column',
  alignItems: 'stretch',
  justifyContent: 'flex-start',
  bgColor: 'transparent',
  absoluteCenterChildren: false,
};

export {
  BoxMargins,
  BoxPadding,
  BoxAlignments,
  BoxDirections,
  BoxInteractions,
  BoxDimensions,
};

export default React.memo(Box);
