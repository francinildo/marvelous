import styled from "styled-components/native";

function getStyle({
  width,
  height,
  flexDirection,
  alignItems,
  justifyContent,
  bgColor,
}) {
  return `
  width: ${width};
  height: ${height};
  flex-direction: ${flexDirection};
  align-items: ${alignItems};
  justify-content: ${justifyContent};
  background-color: ${bgColor};
  `;
}

export const Container = styled.View`
  ${props => getStyle(props)}
`;
export const TouchableContainer = styled.TouchableOpacity`
  ${props => getStyle(props)}
`;
